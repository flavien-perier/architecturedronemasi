---
title: "Agent d'accueil"
date: "11 octobre 2019"
author: "MASI"
lang: "fr-FR"
implicit_figures: true
titlepage: true
titlepage-rule-color: "ff6600"
logo: ./logo.png
toc: true
toc-title: "Table des matières"
toc-own-page: true
---

# Description

L'objectif est de fournir un système permettant de guider des personnes au sein d'un bâtiment à l'aide de drone autonome.

# Équipements

Techno | Prix | Nombre
-|:-:|:-:
[Borne tactile sous Android](https://www.digilor.fr/boutique/supports-interactifs/bornes-tactiles/borne-tactile-android-19-pouces/) |  2690€ | 1
Serveur Linux | 1000€ - 2000€ | 1
[Système de recharge sans fil](https://www.wibotic.com/) | Prototype | 5
[Drone](https://www.studiosport.fr/dji-mavic-2-enterprise-a16810.html?refSrc=AR0041151&nosto=productpage-horizontal-2) | 2000€ | 5
[Dispositif sonore pour le drone](https://www.studiosport.fr/haut-parleur-pour-dji-mavic-2-enterprise-a17038.html) | 100€ | 5
Volets roulants pour petites ouvertures | 500€ | 2-10
Répétiteurs WiFI | 200€ | 3-5

# Interactions

## La borne interactive

Une borne devra être positionnée dans le hall d'entrée de façon à ce que les visiteurs puissent y accéder.

![Borne de recharge](./borne.jpg)

Sur cette dernière les différentes personnes présentent dans le bâtiment sont représentées avec leurs noms. Une barre de recherche peut permettre de trouver plus rapidement une personne en particulier.

![Interface borne](./interfaceBorne.png)

Une fois que le visiteur à sélectionner une personne, une notification est envoyé à cette dernière. S’il répond favorablement à la notification, la borne demande à l'utilisateur s’il veut être accompagné par un drone, et si c'est le cas, une photo devra être prise. Une fois la photo prise le serveur vérifie l'état des drones présents dans le hangar afin de sélectionner celui qui dispose du meilleur niveau de batterie.

![Interface notification](./interfaceBorneNotification.png)

## Le hangar des drones

Les bornes de recharges des drones sont fixées sur une planche positionnée au-dessus de la porte d'entrée. Sur chacune de ces bornes est posé un drone. Cet ensemble forme le hangar à drones.

## Les drones

Une fois en l'air le drone se déplacera de checkpoint en checkpoint. Entre deux checkpoints, le drone avance sans se soucier de l'utilisateur. Une fois arrivé à son checkpoint il se retourne de façon à voir l'avancée de l'utilisateur. Une fois l'utilisateur à proximité le drone se remet dans le sens de la marche et avance jusqu'au checkpoint suivant. Le drone peut donc analyser son environnement entre deux checkpoints sans se soucier de la personne qui le suit.

Les checkpoints seront identifiés à l'aide de QRcode positionnée au niveau des murs. Il serviront également de points de repère pour la localisation du drone dans le bâtiment.

![Exemple de QR code](./qrCode.jpg)

Les drones sont normalement capables d'évaluer leur hauteur par rapport au sol. Cette fonctionnalité pourra donc être utilisée afin de définir des espaces aériens. De cette façon on peut définir 2 espaces entre deux checkpoints : 
- Le plus bas qui sera l'espace des drones à l'aller.
- Le plus haut qui sera l'espace des drones au retour.

Si plusieurs drones sont présents sur un même checkpoint, on fait aller vers le haut tous les drones déjà présents sur cet espace, de sorte qu'ils soient tous positionnés les uns au-dessus des autres.

## Les portes

Au niveau des portes, des systèmes de volets roulants à commande numérique pourront être mis au-dessus du battant, de sorte que les drones puissent passer.

Un checkpoint sera donc positionné avant la porte, afin que le serveur central soit averti de la présence du drone à cet endroit et qu'il ait le temps d'ouvrir le volet pour le laisser passer.

![Volet roulant](./voletRoulant.jpg)

## Le serveur central

Le serveur central devra être relié aux différents répétiteurs WiFi. Il est à noter que la conception des drones fait qu'ils ne sont pilotables que depuis leurs propres réseaux WiFi. Le serveur devra donc être connecté simultanément sur tous les réseaux des drones. Il serait donc préférable que l'OS de ce serveur soit Linux, où la gestion des interfaces WiFi semble plus abordable. Quant à l'application qui va s'occuper de piloter les drones, de gérer la reconaissance faciale et la reconaissance des QR code, il serait préférable qu'elle soit développé dans un language de bas niveau comme le C ou le Rust pour des problématiques temps réel.

## Diagramme de séquence

![Diagramme de séquence récupilatif](./Sequence.png)
