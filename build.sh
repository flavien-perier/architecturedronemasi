#!/bin/bash

cd documents
ls *.md | while read FILE
do
	NBR_LINE=`cat $FILE | wc -c`
	echo "$FILE: $(expr $NBR_LINE / 2250)"
	cat $FILE | pandoc --from markdown -o "../build/$FILE.pdf" --template "../style/eisvogel.latex" --listings -N
done

